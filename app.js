const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const helmet = require("helmet");
const rateLimit = require("express-rate-limit");
const weatherRoutes = require("./routes/weatherRoutes");
const userRoutes = require("./routes/userRoutes");
const cors = require("cors");

// CONFIG
const PORT = process.env.PORT || 9000;

// INIT EXPRESS
const app = express();

// MONGO URL
const databaseName = 'name'
const MONGO_URL = "mongodb://localhost:27017/" + databaseName;

// SECURITY MIDDLEWARES
app.use(helmet());

app.use(
  rateLimit({
    windowMs: 1000 * 60 * 15, // 15 minutes
    max: 1000 // 100 requests/15min
  })
);

// MIDDLEWARES
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(cors());

// Environment variables
require('dotenv').config();

// CONNECT TO DB
mongoose.connect(
  MONGO_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  },
  err => {
    if (!err) {
      console.log("MongoDB Connection Succeeded to db = " + MONGO_URL);
    } else {
      console.log("Error in DB connection: " + err);
    }
  }
);

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

// ADD ROUTES
app.use(weatherRoutes);
app.use(userRoutes);

// START THE SERVER
app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));