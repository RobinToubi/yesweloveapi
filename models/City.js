const mongoose = require("mongoose");

const CitySchema = new mongoose.Schema({
  id: { type: String, required: true, index: true },
  username: { type: String, required: true }
});

module.exports = mongoose.model("City", CitySchema);
