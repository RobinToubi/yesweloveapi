const jwt = require("jsonwebtoken");

// Token verification
const auth = (req, res, next) => {
  const isValidToken = jwt.verify(
    req.headers.authorization,
    process.env.APISECRET
  );
  if (isValidToken)
    next();
  else
    res.status(401).json({
      error: "Authorization required"
    });
};

module.exports = auth;